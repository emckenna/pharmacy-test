function MedCtrl($scope, $http, medList) {
	$scope.meds = medList.query();

	$scope.deleteMed = function(id, index){
		$http.delete('/admin/medicines/'+id).
			success(function(){
				$('.message-block').html('Med Deleted')
					.fadeOut(1800, function(){
						$(this).html('').toggle();
					});
				$scope.meds.splice(index, 1);
			}).
			error(function(data, status, headers, config){
				$('.message-block').html('There was an error: Status:'+status);
			});
	}

}

function MedDetailCtrl($scope, $routeParams, $http) {
	$http.get('/admin/medicines/'+$routeParams.medId + '.json').
		success(function(data){
			$scope.med = data;
		})
}

function MedNewCtrl($scope, $http, medList){
	$scope.title = "New";
	$scope.medSubmit = function(){
		$http.post('/admin/medicines', $scope.med).
			success(function(){
				console.log('success');
				$scope.meds = medList.query();
				window.location = '#/';
			});
	};
}

function MedEditCtrl($scope, $routeParams, $http, $location) {
	$scope.title = "Edit";
	$http.get('/admin/medicines/'+$routeParams.medId + '.json').
		success(function(data){
			$scope.med = data;
		});

	$scope.medSubmit = function(){
		$http.put('/admin/medicines/'+$routeParams.medId, $scope.med).
			success(function(){
				window.location = '#/';
			});
	};
}

