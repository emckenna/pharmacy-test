// Angular SPA with routes
// Templates are contained within script tags on the main app
var myApp = angular.module('pharmapp', ['ngResource'])
	.config(['$routeProvider', function($routeProvider) {
	 	$routeProvider.
	 		when('/', {templateUrl: 'list-med.html', controller: MedCtrl}).
	 		when('/new', {templateUrl: 'new-med.html', controller: MedNewCtrl}).
	 		when('/show/:medId', {templateUrl: 'show-med.html', controller: MedDetailCtrl}).
	 		when('/edit/:medId', {templateUrl: 'new-med.html', controller: MedEditCtrl}).
	 		otherwise({redirecTo: '/admin/medicines'});
	}])
	.config(['$httpProvider', function(p) {
		var m = document.getElementsByTagName('meta');
		for (var i in m) {
			if (m[i].name == 'csrf-token'){
				p.defaults.headers.common['X-CSRF-Token'] = m[i].content;
				break;
			}
		}
	}]);

// service for loading our list
myApp.factory('medList', function($resource){
	return $resource('/admin/medicines.json', {}, {
		query: {method:'GET', params:{}, isArray:true}
	});
});