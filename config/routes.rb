Pharmacy::Application.routes.draw do
  scope "/admin" do
    resources :medicines
  end

  match '/inventory' => 'medicines#inventory'
  match '/order' => 'medicines#order', :via => :put

  root :to => 'medicines#main'
end
