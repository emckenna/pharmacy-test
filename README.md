## Answers.


### 1.  The output is as follows.

if price changes;
<pre><code>Current price: <value>
--- <current_time_at_call>: SMS Alert for price <value>
+++ <current_time_at_call>: Email Alert Price changed to <value>
</code></pre>
if price does not change;
<pre><code>Current price: <value>
</code></pre>

I can't list the actual values since the price Ticket 
is initialized with is changed by a random amount before the value is printed.

### 2. What are callbacks and filters used for in Rails?  Give some examples of where you would use them and how.

In Rails, callbacks are methods provided when using ActiveRecord.  They allow a user to act upon the model before state changes.  A common use would be to handle related model clean up for destroy methods.  Say, School class contains many ClassRoom classes, when deleting a School object, the after_destroy method could be used to remove attached ClassRoom objects.

In Rails, filters are used to target code that should be run before/after a controller does its work.  For example, In a update method of a controller, we can set up a after_filter to send an email to the owner of the model.

### 3. Explain with examples how class and instance variables work in Ruby.
class variables, look like '@@tires'.  Instance variables appear as '@doors'.  The scope is also different, class variables hold one value for all the instances of a class.  Any changes to a class variable affect all sub-classes.  Instance variables are only accessible from within the same object.

example at [gist](https://gist.github.com/emckenna/a59572e5051c43d7872b)

### 4. Didn't get to Ajax admin, time window.
assumed that code blocks were single line and wrapped with brackets.
[Code](https://bitbucket.org/emckenna/pharmacy-test/src)
[heroku](http://reelpharm.herokuapp.com/)

### 5. Did not start, Hit time window.  Was going to to use devise or other authorization gem. 
also utilize some sort of after creation filter for adding names to user list.

