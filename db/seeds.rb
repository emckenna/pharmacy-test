# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Medicine.create(name: 'Tylenol', 
	code_for_placing_order: '{"Ordered Tylenol";}',
	ordered_qty: 0
);
Medicine.create(name: 'Bengay', 
	code_for_placing_order: '{"Ordered Bengay";}',
	ordered_qty: 0
);
Medicine.create(name: 'Neosporin', 
	code_for_placing_order: '{"Ordered Neosporin";}',
	ordered_qty: 0
);
